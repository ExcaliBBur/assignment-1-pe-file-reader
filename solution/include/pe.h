/// @file
/// @brief File contains structure of PE file

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// File offset to the PE signature
#define OFFSET 0x3c
/// A 4-byte signature that identifies the file as a PE format image file
#define MAGIC_VALUE 0x00004550

#ifdef _MSC_VER
	#pragma packed(push, 1)
#endif

/**
 * @brief Structure contains PE header
 * @note This structure is packed for correct work with it.
 **/
struct
#if defined __clang__ || defined __GNUC__
	__attribute__((packed))
#endif
	PEHeader
{
	/// @brief The number that identifies the type of target machine.
	uint16_t machine;
	/// @brief The number of sections. This indicates the size of the section table, which immediately follows the headers.
	uint16_t number_of_sections;
	/// @brief The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created.
	uint32_t time_date_stamp;
	/// @brief The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
	uint32_t pointer_to_symbol_table;
	/// @brief The number of entries in the symbol table.
	uint32_t number_of_symbols;
	/// @brief The size of the optional header, which is required for executable files but not for object files.
	uint16_t size_of_optional_header;
	/// @brief The flags that indicate the attributes of the file.
	uint16_t characteristics;
};

/**
 * @brief Structure contains Section header
 * contains information about each section in PE file
 * @note This structure is packed for correct work with it.
 */
struct 
#if defined __clang__ || defined __GNUC__
	__attribute__((packed))
#endif
	SectionHeader
{
	/// @brief An 8-byte, null-padded UTF-8 encoded string. Section name.
	char name[8];
	/// @brief The total size of the section when loaded into memory.
	uint32_t virtual_size;
	/// @brief For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
	uint32_t virtual_address;
	/// @brief The size of the section (for object files) or the size of the initialized data on disk (for image files).
	uint32_t size_of_raw_data;
	/// @brief The file pointer to the first page of the section within the COFF file.
	uint32_t pointer_to_raw_data;
	/// @brief The file pointer to the beginning of relocation entries for the section.
	uint32_t pointer_to_relocations;
	/// @brief The file pointer to the beginning of line-number entries for the section.
	uint32_t pointer_to_linenumbers;
	/// @brief The number of relocation entries for the section.
	uint16_t number_of_relocations;
	/// @brief The number of line-number entries for the section.
	uint16_t number_of_linenumbers;
	/// @brief The flags that describe the characteristics of the section.
	uint32_t characteristics;
};

#ifdef _MSC_VER
	#pragma packed(pop)
#endif

/**
 * @brief This structure describe PEFile
 */
struct PEFile
{
	/// @brief File magic
	uint32_t magic;
	/// @brief Offset contained by MAGIC_VALUE address
	uint32_t offset;

	/// @name File headers
	///@{

	/// @brief Main header
	struct PEHeader header;
	/// @brief Array of section headers with the size of header.number_of_sections
	struct SectionHeader *section_headers;

	///@}
};

/**
 * @brief Read PE file
 * @param[in] in input file
 * @param[in] pe_file structure describes PE file
 * @return 1 in case of successful reading from file
 */
int8_t read_pe_file(FILE *in, struct PEFile *pe_file);

/**
 * @brief Find section by name
 * @param[in] pe_file structure describes PE file
 * @param[in] name section name
 * @return SectionHeader structure found by name in case of successful found by name
 */
struct SectionHeader *find_section(const struct PEFile *pe_file, const char *name);

/**
 * @brief Write section data to file
 * @param[in] in input file
 * @param[in] out output file
 * @param[in] section SectiorHeader structure contains data
 * @return 1 in case of successful writing to file
 */
int8_t write_section(FILE *in, FILE *out, const struct SectionHeader *section);
