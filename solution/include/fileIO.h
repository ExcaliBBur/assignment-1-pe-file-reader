/// @file
/// @brief File contains input/output file library

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/**
 * @brief Opens a file
 * @param[in] file Double pointer to file
 * @param[in] filename Filename
 * @param[in] open_mode mode to open the file
 * @return 1 if file is opened
 */
bool open_file(FILE **file, char *const filename, char *open_mode);

/**
 * @brief Closes a file
 * @param[in] file Pointer to file
 * @return 1 if file is closed
 */
bool close_file(FILE *file);
