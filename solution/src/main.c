/// @file
/// @brief Main application file

#include "fileIO.h"
#include "pe.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
	fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv)
{
	if (argc != 4)
	{
		usage(stdout);
		return 1;
	}

	FILE *in = NULL;
	FILE *out = NULL;
	if (!open_file(&in, argv[1], "rb"))
	{
		printf("Bad first input file\n");
		return 1;
	}
	if (!open_file(&out, argv[3], "wb"))
	{
		close_file(in);
		printf("Bad second input file\n");
		return 1;
	}
	struct PEFile pe_file;
	if (!read_pe_file(in, &pe_file))
	{
		close_file(in);
		close_file(out);
		free(pe_file.section_headers);
		printf("Incorrect read from file");
		return 1;
	}
	struct SectionHeader *section = find_section(&pe_file, argv[2]);
	if (!section)
	{
		close_file(in);
		close_file(out);
		free(pe_file.section_headers);
		printf("No section with given name");
		return 1;
	}
	if (!write_section(in, out, section))
	{
		close_file(in);
		close_file(out);
		free(pe_file.section_headers);
		printf("Can't write data to file");
		return 1;
	}
	printf("Data written succesfully\n");
	close_file(in);
	close_file(out);
	free(pe_file.section_headers);
	return 0;
}
