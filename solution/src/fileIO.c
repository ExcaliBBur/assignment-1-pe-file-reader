/// @file
/// @brief File contains input/output file library

#include "fileIO.h"

/**
 * @brief Opens a file
 * @param[in] file Double pointer to file
 * @param[in] filename Filename
 * @param[in] open_mode mode to open the file
 * @return 1 if file is opened
 */
bool open_file(FILE **file, char *const filename, char *const open_mode)
{
    *file = fopen(filename, open_mode);
    return (*file != NULL);
}

/**
 * @brief Closes a file
 * @param[in] file Pointer to file
 * @return 1 if file is closed
 */
bool close_file(FILE *file)
{
    return fclose(file) == 0;
}
