/// @file
/// @brief File contains structure of PE file

#include "pe.h"

/**
 * @brief Read PE file
 * @param[in] in input file
 * @param[in] pe_file structure describes PE file
 * @return 1 in case of successful reading from file
 */
int8_t read_pe_file(FILE *in, struct PEFile *pe_file)
{
    if (!in)
        return 0;
    if (fseek(in, OFFSET, SEEK_SET))
        return 0;

    if (!fread(&pe_file->offset, sizeof(uint32_t), 1, in))
        return 0;
    if (fseek(in, pe_file->offset, SEEK_SET))
        return 0;

    if (!fread(&pe_file->magic, sizeof(pe_file->magic), 1, in))
        return 0;
    if (pe_file->magic != MAGIC_VALUE)
        return 0;

    if (!fread(&pe_file->header, sizeof(pe_file->header), 1, in))
        return 0;

    pe_file->section_headers = malloc(pe_file->header.number_of_sections * sizeof(struct SectionHeader));

    if (!pe_file->section_headers)
        return 0;

    if (fseek(in, pe_file->header.size_of_optional_header, SEEK_CUR))
        return 0;

    for (uint16_t i = 0; i < pe_file->header.number_of_sections; i++)
    {
        if (!fread(&pe_file->section_headers[i], sizeof(struct SectionHeader), 1, in))
            return 0;
    }

    return 1;
}

/**
 * @brief Find section by name
 * @param[in] pe_file structure describes PE file
 * @param[in] name section name
 * @return SectionHeader structure found by name in case of successful found by name
 */
struct SectionHeader *find_section(const struct PEFile *pe_file, const char *name)
{
    for (uint16_t i = 0; i < pe_file->header.number_of_sections; i++)
    {
        if (strcmp(name, pe_file->section_headers[i].name) == 0)
            return pe_file->section_headers + i;
    }
    return NULL;
}

/**
 * @brief Write section data to file
 * @param[in] in input file
 * @param[in] out output file
 * @param[in] section SectiorHeader structure contains data
 * @return 1 in case of successful writing to file
 */
int8_t write_section(FILE *in, FILE *out, const struct SectionHeader *section)
{
    if (!in || !out)
        return 0;

    if (section->pointer_to_raw_data == 0)
        return 0;
    if (fseek(in, section->pointer_to_raw_data, SEEK_SET))
        return 0;

    char *data = malloc(section->size_of_raw_data);
    if (!data)
        return 0;

    if (!fread(data, section->size_of_raw_data, 1, in))
        return 0;
    if (!fwrite(data, section->size_of_raw_data, 1, out))
        return 0;
    free(data);

    return 1;
}
